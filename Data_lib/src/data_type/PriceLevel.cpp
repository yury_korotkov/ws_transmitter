#include <data_type\PriceLevel.h>

#include <cmath>

using namespace DATA;

PriceLevel::PriceLevel()
	: _price(NAN), _size(0){
}

PriceLevel::PriceLevel(double price, unsigned size) {
	SetPriceLevel(price, size);
}

PriceLevel::~PriceLevel(){
}

void PriceLevel::SetPriceLevel(double price, unsigned size) {
	_price = price;
	_size = size;
}

void PriceLevel::SetPrice(double price) {
	_price = price;
}

double PriceLevel::GetPrice() {
	return _price;
}

void PriceLevel::SetSize(unsigned size) {
	_size = size;
}

unsigned PriceLevel::GetSize() {
	return _size;
}
