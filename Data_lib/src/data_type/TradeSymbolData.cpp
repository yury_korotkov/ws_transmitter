#include <data_type\TradeSymbolData.h>

#include <cmath>

using namespace DATA;
using namespace TYPE;

TradeSymbol& DATA::TYPE::operator++(TradeSymbol& symbol) {
	switch (symbol) {
	case TS_UNDEFINED: symbol = TS_BTCUSD; break;
	case TS_BTCUSD: symbol = TS_BTCEUR; break;
	case TS_BTCEUR:symbol = TS_LTCBTC; break;
	case TS_LTCBTC: symbol = TS_LTCUSD; break;
	case TS_LTCUSD: symbol = TS_LTCEUR; break;
	case TS_LTCEUR: symbol = TS_DSHBTC; break;
	case TS_DSHBTC: symbol = TS_ETHBTC; break;
	case TS_ETHBTC: symbol = TS_ETHEUR; break;
	case TS_ETHEUR: symbol = TS_NXTBTC; break;
	case TS_NXTBTC: symbol = TS_BCNBTC; break;
	case TS_BCNBTC: symbol = TS_XDNBTC; break;
	case TS_XDNBTC: symbol = TS_DOGEBTC; break;
	case TS_DOGEBTC: symbol = TS_XMRBTC; break;
	case TS_XMRBTC: symbol = TS_QCNBTC; break;
	case TS_QCNBTC: symbol = TS_FCNBTC; break;
	case TS_FCNBTC: symbol = TS_LSKBTC; break;
	case TS_LSKBTC: symbol = TS_LSKEUR; break;
	case TS_LSKEUR: symbol = TS_STEEMBTC; break;
	case TS_STEEMBTC: symbol = TS_STEEMEUR; break;
	case TS_STEEMEUR: symbol = TS_SBDBTC; break;
	case TS_SBDBTC: symbol = TS_DASHBTC; break;
	case TS_DASHBTC: symbol = TS_XEMBTC; break;
	case TS_XEMBTC: symbol = TS_EMCBTC; break;
	case TS_EMCBTC: symbol = TS_SCBTC; break;
	case TS_SCBTC: symbol = TS_ARDRBTC; break;
	case TS_ARDRBTC: symbol = TS_ZECBTC; break;
	case TS_ZECBTC: symbol = TS_WAVESBTC; break;
	case TS_WAVESBTC: symbol = TS_LAST; break;
	case TS_LAST: symbol = TS_UNDEFINED; break;
	default: symbol = TS_UNDEFINED; break;
	}

	return symbol;
}

TradeSymbol DATA::TYPE::operator++(TradeSymbol& symbol, int) {
	TradeSymbol result = symbol;
	++result;
	return result;
}

TradeSymbolData::TradeSymbolData()
	: _name(TS_UNDEFINED) {
}

TradeSymbolData::TradeSymbolData(TradeSymbol name) {
	SetName(name);
}

TradeSymbolData::TradeSymbolData(std::string name) {
	SetName(name);
}

TradeSymbolData::~TradeSymbolData() {
}

bool TradeSymbolData::operator==(std::string name) {
	bool result;

	if (strcmp(name.c_str(), GetNameStr().c_str()) != 0) {
		result = false;
	}
	else {
		result = true;
	}

	return result;
}

void TradeSymbolData::SetName(TradeSymbol name) {
	TradeSymbol result;

	if (TS_UNDEFINED < name && name < TS_LAST) {
		result = name;
	}
	else {
		result = TS_UNDEFINED;
	}

	_name = result;;
}

void TradeSymbolData::SetName(std::string name) {
	TradeSymbol result;

	if (strcmp("BTCUSD", name.c_str()) == 0) {
		result = TS_BTCUSD;
	}
	else if (strcmp("BTCEUR", name.c_str()) == 0) {
		result = TS_BTCEUR;
	}
	else if (strcmp("LTCBTC", name.c_str()) == 0) {
		result = TS_LTCBTC;
	}
	else if (strcmp("LTCUSD", name.c_str()) == 0) {
		result = TS_LTCUSD;
	}
	else if (strcmp("LTCEUR", name.c_str()) == 0) {
		result = TS_LTCEUR;
	}
	else if (strcmp("DSHBTC", name.c_str()) == 0) {
		result = TS_DSHBTC;
	}
	else if (strcmp("ETHBTC", name.c_str()) == 0) {
		result = TS_ETHBTC;
	}
	else if (strcmp("ETHEUR", name.c_str()) == 0) {
		result = TS_ETHEUR;
	}
	else if (strcmp("NXTBTC", name.c_str()) == 0) {
		result = TS_NXTBTC;
	}
	else if (strcmp("BCNBTC", name.c_str()) == 0) {
		result = TS_BCNBTC;
	}
	else if (strcmp("XDNBTC", name.c_str()) == 0) {
		result = TS_XDNBTC;
	}
	else if (strcmp("DOGEBTC", name.c_str()) == 0) {
		result = TS_DOGEBTC;
	}
	else if (strcmp("XMRBTC", name.c_str()) == 0) {
		result = TS_XMRBTC;
	}
	else if (strcmp("QCNBTC", name.c_str()) == 0) {
		result = TS_QCNBTC;
	}
	else if (strcmp("FCNBTC", name.c_str()) == 0) {
		result = TS_FCNBTC;
	}
	else if (strcmp("LSKBTC", name.c_str()) == 0) {
		result = TS_LSKBTC;
	}
	else if (strcmp("LSKEUR", name.c_str()) == 0) {
		result = TS_LSKEUR;
	}
	else if (strcmp("STEEMBTC", name.c_str()) == 0) {
		result = TS_STEEMBTC;
	}
	else if (strcmp("STEEMEUR", name.c_str()) == 0) {
		result = TS_STEEMEUR;
	}
	else if (strcmp("SBDBTC", name.c_str()) == 0) {
		result = TS_SBDBTC;
	}
	else if (strcmp("DASHBTC", name.c_str()) == 0) {
		result = TS_DASHBTC;
	}
	else if (strcmp("XEMBTC", name.c_str()) == 0) {
		result = TS_XEMBTC;
	}
	else if (strcmp("EMCBTC", name.c_str()) == 0) {
		result = TS_EMCBTC;
	}
	else if (strcmp("SCBTC", name.c_str()) == 0) {
		result = TS_SCBTC;
	}
	else if (strcmp("ARDRBTC", name.c_str()) == 0) {
		result = TS_ARDRBTC;
	}
	else if (strcmp("ZECBTC", name.c_str()) == 0) {
		result = TS_ZECBTC;
	}
	else if (strcmp("WAVESBTC", name.c_str()) == 0) {
		result = TS_WAVESBTC;
	}
	else {
		result = TS_UNDEFINED;
	}

	_name = result;
}

TradeSymbol TradeSymbolData::GetName() {
	return _name;
}

std::string TradeSymbolData::GetNameStr() {
	std::string result;

	switch (_name)
	{
	case DATA::TYPE::TS_BTCUSD: result = "BTCUSD"; break;
	case DATA::TYPE::TS_BTCEUR: result = "BTCEUR"; break;
	case DATA::TYPE::TS_LTCBTC: result = "LTCBTC"; break;
	case DATA::TYPE::TS_LTCUSD: result = "LTCUSD"; break;
	case DATA::TYPE::TS_LTCEUR: result = "LTCEUR"; break;
	case DATA::TYPE::TS_DSHBTC: result = "DSHBTC"; break;
	case DATA::TYPE::TS_ETHBTC: result = "ETHBTC"; break;
	case DATA::TYPE::TS_ETHEUR: result = "ETHEUR"; break;
	case DATA::TYPE::TS_NXTBTC: result = "NXTBTC"; break;
	case DATA::TYPE::TS_BCNBTC: result = "BCNBTC"; break;
	case DATA::TYPE::TS_XDNBTC: result = "XDNBTC"; break;
	case DATA::TYPE::TS_DOGEBTC: result = "DOGEBTC"; break;
	case DATA::TYPE::TS_XMRBTC: result = "XMRBTC"; break;
	case DATA::TYPE::TS_QCNBTC: result = "QCNBTC"; break;
	case DATA::TYPE::TS_FCNBTC: result = "FCNBTC"; break;
	case DATA::TYPE::TS_LSKBTC: result = "LSKBTC"; break;
	case DATA::TYPE::TS_LSKEUR: result = "LSKEUR"; break;
	case DATA::TYPE::TS_STEEMBTC: result = "STEEMBTC"; break;
	case DATA::TYPE::TS_STEEMEUR: result = "STEEMEUR"; break;
	case DATA::TYPE::TS_SBDBTC: result = "SBDBTC"; break;
	case DATA::TYPE::TS_DASHBTC: result = "DASHBTC"; break;
	case DATA::TYPE::TS_XEMBTC: result = "XEMBTC"; break;
	case DATA::TYPE::TS_EMCBTC: result = "EMCBTC"; break;
	case DATA::TYPE::TS_SCBTC: result = "SCBTC"; break;
	case DATA::TYPE::TS_ARDRBTC: result = "ARDRBTC"; break;
	case DATA::TYPE::TS_ZECBTC: result = "ZECBTC"; break;
	case DATA::TYPE::TS_WAVESBTC: result = "WAVESBTC"; break;
	default:
		result = "UNDEFINED";
		break;
	}

	return result;
}

CurrencySymbol TradeSymbolData::GetMasterSymbol() {
	CurrencySymbol result;

	switch (_name)
	{
	case TS_BTCUSD:
	case TS_BTCEUR:
		result = CS_BTC; break;
	case TS_LTCBTC:
	case TS_LTCUSD:
	case TS_LTCEUR:
		result = CS_LTC; break;
	case TS_DSHBTC:
		result = CS_DSH; break;
	case TS_ETHBTC:
	case TS_ETHEUR:
		result = CS_ETH; break;
	case TS_NXTBTC:
		result = CS_NXT; break;
	case TS_BCNBTC:
		result = CS_BCN; break;
	case TS_XDNBTC:
		result = CS_XDN; break;
	case TS_DOGEBTC:
		result = CS_DOGE; break;
	case TS_XMRBTC:
		result = CS_XMR; break;
	case TS_QCNBTC:
		result = CS_QCN; break;
	case TS_FCNBTC:
		result = CS_FCN; break;
	case TS_LSKBTC:
		result = CS_LSK; break;
	case TS_LSKEUR:
		result = CS_LSK; break;
	case TS_STEEMBTC:
	case TS_STEEMEUR:
		result = CS_STEEM; break;
	case TS_SBDBTC:
		result = CS_SBD; break;
	case TS_DASHBTC:
		result = CS_DASH; break;
	case TS_XEMBTC:
		result = CS_XEM; break;
	case TS_EMCBTC:
		result = CS_EMC; break;
	case TS_SCBTC:
		result = CS_SC; break;
	case TS_ARDRBTC:
		result = CS_ARDR; break;
	case TS_ZECBTC:
		result = CS_ZEC; break;
	case TS_WAVESBTC:
		result = CS_WAVES; break;
	default:
		result = CS_UNDEFINED; break;
	}

	return result;
}

CurrencySymbol TradeSymbolData::GetSlaveSynbol() {
	CurrencySymbol result;

	switch (_name)
	{
	case TS_BTCUSD:
	case TS_LTCUSD:
		result = CS_USD; break;
	case TS_BTCEUR:
	case TS_LTCEUR:
	case TS_ETHEUR:
	case TS_LSKEUR:
	case TS_STEEMEUR:
		result = CS_EUR; break;
	case TS_LTCBTC:
	case TS_DSHBTC:
	case TS_ETHBTC:
	case TS_NXTBTC:
	case TS_BCNBTC:
	case TS_XDNBTC:
	case TS_DOGEBTC:
	case TS_XMRBTC:
	case TS_QCNBTC:
	case TS_FCNBTC:
	case TS_LSKBTC:
	case TS_STEEMBTC:
	case TS_SBDBTC:
	case TS_DASHBTC:
	case TS_XEMBTC:
	case TS_EMCBTC:
	case TS_SCBTC:
	case TS_ARDRBTC:
	case TS_ZECBTC:
	case TS_WAVESBTC:
		result = CS_BTC; break;
	default:
		result = CS_UNDEFINED; break;
	}

	return result;
}

double TradeSymbolData::GetLotSize() {
	double result;

	switch (_name)
	{
	case TS_BTCUSD:
	case TS_BTCEUR:
	case TS_XMRBTC:
	case TS_QCNBTC:
	case TS_FCNBTC:
	case TS_WAVESBTC:
		result = 0.01; break;
	case TS_LTCBTC:
	case TS_LTCUSD:
	case TS_LTCEUR:
	case TS_EMCBTC:
		result = 0.1; break;
	case TS_DSHBTC:
	case TS_ARDRBTC:
	case TS_NXTBTC:
	case TS_LSKBTC:
	case TS_LSKEUR:
	case TS_XEMBTC:
		result = 1.0; break;
	case TS_ETHBTC:
	case TS_ETHEUR:
	case TS_STEEMBTC:
	case TS_STEEMEUR:
	case TS_SBDBTC:
	case TS_DASHBTC:
	case TS_ZECBTC:
		result = 0.001; break;
	case TS_BCNBTC:
	case TS_XDNBTC:
	case TS_SCBTC:
		result = 100.0; break;
	case TS_DOGEBTC:
		result = 1000.0; break;
	default:
		result = NAN; break;
	}

	return result;
}

double TradeSymbolData::GetPriceStep() {
	double result;

	switch (_name)
	{
	case TS_BTCUSD:
	case TS_BTCEUR:
		result = 0.01; break;
	case TS_LTCBTC:
	case TS_STEEMBTC:
	case TS_SBDBTC:
		result = 0.00001; break;
	case TS_LTCUSD:
	case TS_LTCEUR:
		result = 0.001; break;
	case TS_DSHBTC:
	case TS_NXTBTC:
	case TS_XEMBTC:
	case TS_EMCBTC:
		result = 0.00000001; break;
	case TS_ETHBTC:
	case TS_XMRBTC:
	case TS_QCNBTC:
	case TS_FCNBTC:
	case TS_DASHBTC:
	case TS_ZECBTC:
		result = 0.000001; break;
	case TS_ETHEUR:
	case TS_STEEMEUR:
	case TS_LSKEUR:
		result = 0.0001; break;
	case TS_BCNBTC:
	case TS_XDNBTC:
		result = 0.0000000001; break;
	case TS_DOGEBTC:
	case TS_SCBTC:
	case TS_ARDRBTC:
		result = 0.000000001; break;
	case TS_LSKBTC:
	case TS_WAVESBTC:
		result = 0.0000001; break;
	default:
		result = NAN; break;
	}

	return result;
}