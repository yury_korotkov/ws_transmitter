#include <data_type\PriceLevelTrade.h>

using namespace DATA;

PriceLevelTrade::PriceLevelTrade()
	: PriceLevel(), _timeStamp(0) {
}

PriceLevelTrade::PriceLevelTrade(double price, unsigned size, unsigned long timeStamp)
	: PriceLevel(price, size),  _timeStamp(timeStamp){
}

PriceLevelTrade::~PriceLevelTrade() {
}

void PriceLevelTrade::SetPriceLevelTrade(double price, unsigned size, unsigned long timeStamp) {
	PriceLevel(price, size);
	_timeStamp = timeStamp;
}

void PriceLevelTrade::SetTimeStamp(unsigned long timeStamp) {
	_timeStamp = timeStamp;
}

unsigned long PriceLevelTrade::GetTimeStamp() {
	return _timeStamp;
}
