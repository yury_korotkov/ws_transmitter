#include <data_type\TS_MarketData.h>

#include <cmath>
#include <limits>

using namespace DATA;
using namespace TYPE;

TS_MarketData::TS_MarketData()
	: _symbol(TS_UNDEFINED) {
	_accuracy = _symbol.GetPriceStep() < 0 ? 1 / _symbol.GetPriceStep() : _symbol.GetSlaveSynbol();
}

TS_MarketData::TS_MarketData(TradeSymbol symbol)
	: _symbol(TradeSymbolData(symbol)) {
	_accuracy = _symbol.GetPriceStep() < 0 ? 1 / _symbol.GetPriceStep() : _symbol.GetSlaveSynbol();
}

TS_MarketData::~TS_MarketData() {
	ClearAsk();
	ClearBid();
}

void TS_MarketData::SetTradeSymbol(TradeSymbolData symbol) {
	_symbol = symbol;
}

TradeSymbolData TS_MarketData::GetTradeSymbol() {
	return _symbol;
}

void TS_MarketData::SetExchangeStatus(ExchangeStatusData status) {
	_status = status;
}

void TS_MarketData::SetExchangeStatus(std::string status) {
	_status = ExchangeStatusData(status);
}

ExchangeStatusData TS_MarketData::GetExchangeStatus() {
	return _status;
}

void TS_MarketData::AppendAsk(PriceLevel level) {
	bool needAppend = true;

	for (auto it : _ask) {
		bool isOverlap = it->GetPrice() * _accuracy == level.GetPrice() * _accuracy;
		if (isOverlap) {
			it->SetSize(it->GetSize() + level.GetSize());
			needAppend = false;
			break;
		}
	}

	if (needAppend) {
		_ask.push_back(new PriceLevel(level));
	}
}

void TS_MarketData::AppendAsk(std::string price, std::string size) {
	bool needAppend = true;

	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _ask) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {
			it->SetSize(it->GetSize() + sizeVal);
			needAppend = false;
			break;
		}
	}

	if (needAppend) {
		_ask.push_back(new PriceLevel(PriceLevel(priceVal, sizeVal)));
	}
}

void TS_MarketData::ReplaceAsk(std::string price, std::string size) {
	bool needAppend = true;

	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _ask) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {
			it->SetSize(sizeVal);
			needAppend = false;
			break;
		}
	}

	if (needAppend && sizeVal != 0) {
		_ask.push_back(new PriceLevel(PriceLevel(priceVal, sizeVal)));
	}
}

void TS_MarketData::ExcludeAsk(std::string price, std::string size) {
	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _ask) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {

			if (sizeVal <= it->GetSize()) {
				it->SetSize(it->GetSize() - sizeVal);
			}
			else {
				it->SetSize(0);
			}
			break;
		}
	}
}

std::vector<PriceLevel*> TS_MarketData::GetAsk() {
	return _ask;
}

void TS_MarketData::ClearAsk() {
	for (auto it : _ask) {
		delete it;
	}

	_ask.clear();
}

size_t TS_MarketData::CountAsk() {
	size_t result = 0;
	for (size_t i = 0; i < _ask.size(); ++i){
		result += _ask.at(i)->GetSize();
	}
	return result;
}

size_t TS_MarketData::GetAskLots() {
	size_t result = 0;
	for (size_t i = 0; i < _ask.size(); ++i) {
		result += _ask.at(i)->GetSize();
	}

	return result;
}

double TS_MarketData::GetLessAsk() {
	double result = std::numeric_limits<double>::max();
	for (size_t i = 0; i < _ask.size(); i++){
		if (_ask.at(i)->GetPrice() < result && _ask.at(i)->GetSize() > 0) {
			result = _ask.at(i)->GetPrice();
			break;
		}
	}

	return result != std::numeric_limits<double>::max() ? result : NAN;
}

double TS_MarketData::GetMiddleAsk() {
	double result = 0;
	size_t size = CountAsk();

	for (size_t i = 0; i < _ask.size(); i++){
		if (_ask.at(i)->GetSize() != 0) {
			result += _ask.at(i)->GetPrice() * _ask.at(i)->GetSize();
		}
	}

	return size > 0 ? result / size : NAN;
}

double TS_MarketData::GetMoreAsk() {
	double result = std::numeric_limits<double>::min();
	for (size_t i = 0; i < _ask.size(); i++) {
		if (_ask.at(i)->GetPrice() > result && _ask.at(i)->GetSize() > 0) {
			result = _ask.at(i)->GetPrice();
			break;
		}
	}

	return result != std::numeric_limits<double>::min() ? result : NAN;
}

void TS_MarketData::AppendBid(PriceLevel level) {
	bool needAppend = true;

	for (auto it : _bid) {
		bool isOverlap = it->GetPrice() * _accuracy == level.GetPrice() * _accuracy;
		if (isOverlap) {
			it->SetSize(it->GetSize() + level.GetSize());
			needAppend = false;
			break;
		}
	}

	if (needAppend) {
		_bid.push_back(new PriceLevel(level));
	}
}

void TS_MarketData::AppendBid(std::string price, std::string size) {
	bool needAppend = true;

	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _bid) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {
			it->SetSize(it->GetSize() + sizeVal);
			needAppend = false;
			break;
		}
	}

	if (needAppend) {
		_bid.push_back(new PriceLevel(PriceLevel(priceVal, sizeVal)));
	}
}

void TS_MarketData::ReplaceBid(std::string price, std::string size) {
	bool needAppend = true;

	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _bid) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {
			it->SetSize(sizeVal);
			needAppend = false;
			break;
		}
	}

	if (needAppend && sizeVal != 0) {
		_bid.push_back(new PriceLevel(PriceLevel(priceVal, sizeVal)));
	}
}

void TS_MarketData::ExcludeBid(std::string price, std::string size) {
	double priceVal = std::stod(price);
	unsigned sizeVal = std::stoi(size);

	for (auto it : _bid) {
		bool isOverlap = it->GetPrice() * _accuracy == priceVal * _accuracy;
		if (isOverlap) {

			if (sizeVal <= it->GetSize()) {
				it->SetSize(it->GetSize() - sizeVal);
			}
			else {
				it->SetSize(0);
			}
			break;
		}
	}
}


std::vector<PriceLevel*> TS_MarketData::GetBid() {
	return _bid;
}

void TS_MarketData::ClearBid() {
	for (auto it : _bid) {
		delete it;
	}

	_bid.clear();
}

size_t TS_MarketData::CountBid() {
	size_t result = 0;
	for (size_t i = 0; i < _bid.size(); ++i) {
		result += _bid.at(i)->GetSize();
	}

	return result;
}

size_t TS_MarketData::GetBidLots() {
	size_t result = 0;
	for (size_t i = 0; i < _bid.size(); ++i) {
		result += _bid.at(i)->GetSize();
	}

	return result;
}

double TS_MarketData::GetLessBid() {
	double result = std::numeric_limits<double>::max();
	for (size_t i = 0; i < _bid.size(); i++) {
		if (_bid.at(i)->GetPrice() < result && _bid.at(i)->GetSize() > 0) {
			result = _bid.at(i)->GetPrice();
			break;
		}
	}

	return result != std::numeric_limits<double>::max() ? result : NAN;
}

double TS_MarketData::GetMiddleBid() {
	double result = 0;
	size_t size = CountAsk();

	for (size_t i = 0; i < _bid.size(); i++) {
		if (_bid.at(i)->GetSize() != 0) {
			result += _bid.at(i)->GetPrice() * _bid.at(i)->GetSize();
		}
	}

	return size > 0 ? result / size : NAN;
}

double TS_MarketData::GetMoreBid() {
	double result = std::numeric_limits<double>::min();
	for (size_t i = 0; i < _bid.size(); i++) {
		if (_bid.at(i)->GetPrice() > result && _bid.at(i)->GetSize() > 0) {
			result = _bid.at(i)->GetPrice();
			break;
		}
	}

	return result != std::numeric_limits<double>::min() ? result : NAN;
}