#include <data_type\ExchangeStatusData.h>

using namespace DATA;
using namespace TYPE;

ExchangeStatusData::ExchangeStatusData() 
	: _name(ESN_UNDEFINED){
}

ExchangeStatusData::ExchangeStatusData(ExchangeStatus name) {
	SetStatusName(name);
}

ExchangeStatusData::ExchangeStatusData(std::string name) {
	SetStatusName(name);
}

ExchangeStatusData::~ExchangeStatusData() {
}

void ExchangeStatusData::SetStatusName(ExchangeStatus name) {
	ExchangeStatus result;

	if (ESN_UNDEFINED < name && name < ESN_LAST) {
		result = name;
	}
	else {
		result = ESN_UNDEFINED;
	}

	_name = result;
}

void ExchangeStatusData::SetStatusName(std::string name) {
	ExchangeStatus result;

	if (strcmp("working", name.c_str()) == 0) {
		result = ESN_WORKING;
	}
	else {
		result = ESN_UNDEFINED;
	}

	_name = result;
}

ExchangeStatus ExchangeStatusData::GetStatusName() {
	return _name;
}