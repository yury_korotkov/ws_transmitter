#include <export_data\MarketData.h>

#include <data_type\TradeSymbolData.h>

using namespace DATA;
using namespace TYPE;

MarketData::MarketData(){
	Initialization();
}

MarketData::~MarketData(){
	for (auto it : _marketData) {
		delete it;
	}
}

TS_MarketData* MarketData::GetTradeSymbol(TYPE::TradeSymbol symbol) {
	TS_MarketData *result = NULL;

	for (TS_MarketData* it : _marketData) {
		if (it->GetTradeSymbol().GetName() == symbol) {
			result = it;
			break;
		}
	}

	return result;
}

TS_MarketData* MarketData::GetTradeSymbol(std::string symbol) {
	TS_MarketData *result = NULL;

	for (TS_MarketData* it : _marketData) {
		if (it->GetTradeSymbol() == symbol) {
			result = it;
			break;
		}
	}

	return result;
}

void MarketData::Initialization() {
	for (TradeSymbol index = TradeSymbol(1); index < TS_LAST; ++index) {
		TS_MarketData *new_Data = new TS_MarketData(index);
		_marketData.push_back(new_Data);
	}
}
