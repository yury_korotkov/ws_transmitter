#include <export_data\StreamFrame.h>

using namespace DATA;

StreamFrame::StreamFrame() {
}

StreamFrame::~StreamFrame() {
	for (size_t index = 0; index < _frameQueue.size(); ++index) {
		delete _frameQueue.front();
		_frameQueue.pop();
	}
}

void StreamFrame::PushBack(std::string* frame) {
	if (frame != NULL) {
		_frameQueue.push(frame);
	}
}

std::string* StreamFrame::Front() {
	std::string *result;

	if (_frameQueue.size() != 0) {
		result = _frameQueue.front();
	}
	else {
		result = new std::string;
	}

	return result;
}

void StreamFrame::PopFront() {
	if (_frameQueue.size() != 0) {
		_frameQueue.pop();
	}
}

size_t StreamFrame::Size() {
	return _frameQueue.size();
}