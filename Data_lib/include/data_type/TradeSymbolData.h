#pragma once

#include <string>

namespace DATA {

	namespace TYPE {

		enum TradeSymbol {
			TS_UNDEFINED = 0,
			TS_BTCUSD,
			TS_BTCEUR,
			TS_LTCBTC,
			TS_LTCUSD,
			TS_LTCEUR,
			TS_DSHBTC,
			TS_ETHBTC,
			TS_ETHEUR,
			TS_NXTBTC,
			TS_BCNBTC,
			TS_XDNBTC,
			TS_DOGEBTC,
			TS_XMRBTC,
			TS_QCNBTC,
			TS_FCNBTC,
			TS_LSKBTC,
			TS_LSKEUR,
			TS_STEEMBTC,
			TS_STEEMEUR,
			TS_SBDBTC,
			TS_DASHBTC,
			TS_XEMBTC,
			TS_EMCBTC,
			TS_SCBTC,
			TS_ARDRBTC,
			TS_ZECBTC,
			TS_WAVESBTC,
			TS_LAST
		};

		TradeSymbol& operator++(TradeSymbol& symbol);
		TradeSymbol operator++(TradeSymbol& symbol, int);

		enum CurrencySymbol {
			CS_UNDEFINED = 0,
			CS_USD,
			CS_EUR,
			CS_BTC,
			CS_LTC,
			CS_DSH,
			CS_ETH,
			CS_NXT,
			CS_BCN,
			CS_XDN,
			CS_DOGE,
			CS_XMR,
			CS_QCN,
			CS_FCN,
			CS_LSK,
			CS_STEEM,
			CS_SBD,
			CS_DASH,
			CS_XEM,
			CS_EMC,
			CS_SC,
			CS_ARDR,
			CS_ZEC,
			CS_WAVES,
			CS_LAST
		};

	}

	class TradeSymbolData {
	public:
		TradeSymbolData();
		explicit TradeSymbolData(TYPE::TradeSymbol name);
		explicit TradeSymbolData(std::string name);
		virtual ~TradeSymbolData();

		bool operator==(std::string name);

		void SetName(TYPE::TradeSymbol name);
		void SetName(std::string name);
		TYPE::TradeSymbol GetName();
		std::string GetNameStr();

		TYPE::CurrencySymbol GetMasterSymbol();
		TYPE::CurrencySymbol GetSlaveSynbol();

		double GetLotSize();
		double GetPriceStep();

	private:
		TYPE::TradeSymbol _name;
	};

}