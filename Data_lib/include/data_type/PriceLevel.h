#pragma once

namespace DATA {

	class PriceLevel {
	public:
		PriceLevel();
		explicit PriceLevel(double price, unsigned size);
		virtual ~PriceLevel();

		void SetPriceLevel(double price, unsigned size);

		void SetPrice(double price);
		double GetPrice();

		void SetSize(unsigned size);
		unsigned GetSize();

	private:
		double _price;
		unsigned _size;
	};

}


