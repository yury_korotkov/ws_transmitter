#pragma once

#include <data_type\TS_MarketData.h>
#include <data_type\PriceLevelTrade.h>

namespace DATA {

	class TS_MarketDataIncrement : public TS_MarketData {
	public:
		TS_MarketDataIncrement();
		virtual ~TS_MarketDataIncrement();

	protected:
		std::vector<PriceLevelTrade> _trade;
	};

}

