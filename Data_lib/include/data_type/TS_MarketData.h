#pragma once

#include <data_type\TradeSymbolData.h>
#include <data_type\ExchangeStatusData.h>
#include <data_type\PriceLevel.h>

#include <vector>
#include <string>

namespace DATA {

	class TS_MarketData
	{
	public:
		TS_MarketData();
		explicit TS_MarketData(TYPE::TradeSymbol symbol);
		virtual ~TS_MarketData();

		void SetTradeSymbol(TradeSymbolData symbol);
		TradeSymbolData GetTradeSymbol();

		void SetExchangeStatus(ExchangeStatusData status);
		void SetExchangeStatus(std::string status);
		ExchangeStatusData GetExchangeStatus();

		void AppendAsk(PriceLevel level);
		void AppendAsk(std::string price, std::string size);
		void ReplaceAsk(std::string price, std::string size);
		void ExcludeAsk(std::string price, std::string size);
		std::vector<PriceLevel*> GetAsk();
		void ClearAsk();
		size_t CountAsk();
		rsize_t GetAskLots();
		double GetLessAsk();
		double GetMiddleAsk();
		double GetMoreAsk();

		void AppendBid(PriceLevel);
		void AppendBid(std::string price, std::string size);
		void ReplaceBid(std::string price, std::string size);
		void ExcludeBid(std::string price, std::string size);
		std::vector<PriceLevel*> GetBid();
		void ClearBid();
		size_t CountBid();
		size_t GetBidLots();
		double GetLessBid();
		double GetMiddleBid();
		double GetMoreBid();

	protected:
		TradeSymbolData _symbol;
		unsigned _accuracy;
		ExchangeStatusData _status;
		std::vector<PriceLevel*> _ask;
		std::vector<PriceLevel*> _bid;
	};

}