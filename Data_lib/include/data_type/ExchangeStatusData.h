#pragma once

#include <string>

namespace DATA {
	namespace TYPE {

		enum ExchangeStatus {
			ESN_UNDEFINED = 0,
			ESN_WORKING,
			ESN_LAST
		};

	}

	class ExchangeStatusData {
	public:
		ExchangeStatusData();
		explicit ExchangeStatusData(TYPE::ExchangeStatus name);
		explicit ExchangeStatusData(std::string name);
		virtual ~ExchangeStatusData();

		void SetStatusName(TYPE::ExchangeStatus name);
		void SetStatusName(std::string name);
		TYPE::ExchangeStatus GetStatusName();

	private:
		TYPE::ExchangeStatus _name;
	};


}