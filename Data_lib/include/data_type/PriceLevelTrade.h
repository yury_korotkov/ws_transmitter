#pragma once

#include <data_type\PriceLevel.h>

namespace DATA {

	class PriceLevelTrade : public PriceLevel {
	public:
		PriceLevelTrade();
		explicit PriceLevelTrade(double price, unsigned size, unsigned long timeStamp);
		virtual ~PriceLevelTrade();

		void SetPriceLevelTrade(double price, unsigned size, unsigned long timeStamp);

		void SetTimeStamp(unsigned long timeStamp);
		unsigned long GetTimeStamp();

	private:
		unsigned long _timeStamp;
	};

}
