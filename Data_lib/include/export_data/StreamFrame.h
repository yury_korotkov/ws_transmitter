#pragma once

#include <boost\thread\mutex.hpp>
#include <queue>

namespace DATA {

	class StreamFrame
	{
	public:
		StreamFrame();
		virtual ~StreamFrame();

		void PushBack(std::string* frame);
		std::string* Front();
		// Memory not free
		void PopFront(); 

		size_t Size();

	private:
		std::queue<std::string*> _frameQueue;
	};

}



