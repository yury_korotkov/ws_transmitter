#pragma once

#include <data_type\TS_MarketData.h>
#include <vector>
#include <string>

namespace DATA {

	class MarketData
	{
	public:
		MarketData();
		virtual ~MarketData();

		TS_MarketData* GetTradeSymbol(TYPE::TradeSymbol symbol);
		TS_MarketData* GetTradeSymbol(std::string symbol);

	private:
		void Initialization();

	private:
		std::vector<TS_MarketData*> _marketData;
	};

}


