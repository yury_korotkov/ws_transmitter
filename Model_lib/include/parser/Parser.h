#pragma once

#include <IncludeData.h>

#include <boost\thread.hpp>

namespace MODEL {

	class Parser
	{
	public:
		Parser(DATA::StreamFrame *streamBox, boost::mutex *streamBoxMutex,
			DATA::MarketData *marketBox, boost::mutex *marketBoxMutex);
		virtual ~Parser();

		void Run();
		void Stop();

	private:
		boost::mutex *_streamBoxMutex;
		DATA::StreamFrame *_streamBox;

		DATA::MarketData *_marketBox;
		boost::mutex *_marketBoxMutex;

		boost::thread *_thread;
	};

}


