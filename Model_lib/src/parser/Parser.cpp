#include <parser\Parser.h>

#include <boost\chrono.hpp>
#include <rapidjson\document.h>
#include <rapidjson\writer.h>
#include <rapidjson\stringbuffer.h>
#include <rapidjson\filereadstream.h>

#include <string>
#include <iostream>

boost::mutex g_thisThreadMutex;

boost::mutex *g_streamBoxMutex;
DATA::StreamFrame *g_streamBox;

boost::mutex *g_marketBoxMutex;
DATA::MarketData *g_marketBox;

using namespace rapidjson;

void RunThread() {
	std::string testFullFrameStr = "{\"MarketDataSnapshotFullRefresh\":";
	std::string testIncrementFrameStr = "{\"MarketDataIncrementalRefresh\":";

	while (true)
	{
		g_thisThreadMutex.lock();

		g_streamBoxMutex->lock();

		std::string *frame = g_streamBox->Front();
		g_streamBox->PopFront();
		size_t frameNumber = g_streamBox->Size();

		g_streamBoxMutex->unlock();

		const char *test_a = frame->c_str();

		StringBuffer parsStringBuf;
		Writer<StringBuffer> parseWriter(parsStringBuf);

		Document d;
		d.Parse(frame->c_str());

		if (memcmp(testIncrementFrameStr.c_str(), frame->c_str(), testIncrementFrameStr.size()) == 0) {

			Value& root = d["MarketDataIncrementalRefresh"];

			if (root.IsObject()) {
				g_marketBoxMutex->lock();

				Value& symbol = root["symbol"];
				if (symbol.IsString()) {

					DATA::TS_MarketData *data = g_marketBox->GetTradeSymbol(symbol.GetString());
					if (data != NULL) {

						Value& status = root["exchangeStatus"];
						if (status.IsString()) {
							data->SetExchangeStatus(status.GetString());
						}
						else {
							data->SetExchangeStatus("error");
						}

						Value& ask = root["ask"];
						if (ask.IsArray()) {
							for (auto& it : ask.GetArray()) {
								Value& price = it["price"];
								Value& size = it["size"];
								if (price.IsString() && size.IsInt()) {
									data->ReplaceAsk(price.GetString(), std::to_string(size.GetInt()));
								}

							}
						}

						Value& bid = root["bid"];
						if (bid.IsArray()) {
							for (auto& it : bid.GetArray()) {
								Value& price = it["price"];
								Value& size = it["size"];
								if (price.IsString() && size.IsInt()) {
									data->ReplaceBid(price.GetString(), std::to_string(size.GetInt()));
								}

							}
						}

						Value& trade = root["trade"];
						if (trade.IsArray()) {
							for (auto& it : trade.GetArray()) {
								int test_ = frame->size();
								Value& price = it["price"];
								Value& size = it["size"];
								if (price.IsString() && size.IsInt()) {
									Value& side = it["side"];
									if (side.IsString()) {
										if (memcmp("buy", side.GetString(), 3) == 0) {
											data->ExcludeBid(price.GetString(), std::to_string(size.GetInt()));
										}
										else if(memcmp("sell", side.GetString(), 3) == 0){
											data->ExcludeAsk(price.GetString(), std::to_string(size.GetInt()));
										}
									}
								}
							}
						}

					}
				}

				g_marketBoxMutex->unlock();
			}
		}
		else if (memcmp(testFullFrameStr.c_str(), frame->c_str(), testFullFrameStr.size()) == 0) {

			Value& root = d["MarketDataSnapshotFullRefresh"];

			if (root.IsObject()) {
				g_marketBoxMutex->lock();

				Value& symbol = root["symbol"];
				if (symbol.IsString()) {

					DATA::TS_MarketData *data = g_marketBox->GetTradeSymbol(symbol.GetString());
					if (data != NULL) {

						Value& status = root["exchangeStatus"];
						if (status.IsString()) {
							data->SetExchangeStatus(status.GetString());
						}
						else {
							data->SetExchangeStatus("error");
						}

						data->ClearAsk();
						Value& ask = root["ask"];
						if (ask.IsArray()) {
							for (auto& it : ask.GetArray()) {
								Value& price = it["price"];
								Value& size = it["size"];
								if (price.IsString() && size.IsInt()) {
									data->AppendAsk(price.GetString(), std::to_string(size.GetInt()));
								}
							}
						}

						data->ClearBid();
						Value& bid = root["bid"];
						if (bid.IsArray()) {
							for (auto& it : bid.GetArray()) {
								Value& price = it["price"];
								Value& size = it["size"];
								if (price.IsString() && size.IsInt()) {
									data->AppendBid(price.GetString(), std::to_string(size.GetInt()));
								}
							}
						}
					}
				}

				g_marketBoxMutex->unlock();
			}
		}

		delete frame;

		g_thisThreadMutex.unlock();

		if (frameNumber == 0) {
			boost::this_thread::sleep_for(boost::chrono::milliseconds(1));
		}
	}
}

using namespace MODEL;

Parser::Parser(DATA::StreamFrame *streamBox, boost::mutex *streamBoxMutex,
	DATA::MarketData *marketBox, boost::mutex *marketBoxMutex)
	: _streamBoxMutex(streamBoxMutex), _streamBox(streamBox),
	_marketBox(marketBox), _marketBoxMutex(marketBoxMutex), _thread(NULL) {
}


Parser::~Parser() {
	g_thisThreadMutex.lock();

	if (_thread != NULL) {
		_thread->interrupt();
		delete _thread;
	}

	g_thisThreadMutex.unlock();
}

void Parser::Run() {
	g_streamBox = _streamBox;
	g_streamBoxMutex = _streamBoxMutex;

	g_marketBox = _marketBox;
	g_marketBoxMutex = _marketBoxMutex;

	_thread = new boost::thread(&RunThread);
}

void Parser::Stop() {
	g_thisThreadMutex.lock();

	_thread->interrupt();
	delete _thread;
	_thread = NULL;

	g_thisThreadMutex.unlock();
}