#include <web_socket\WS_ClientThread.h>

#include <IncludeData.h>
#include <queue>

#include <websocketpp\config\asio_no_tls_client.hpp>
#include <websocketpp\client.hpp>

#if 0

#include <boost\chrono.hpp> 

boost::chrono::high_resolution_clock::time_point t1
= boost::chrono::high_resolution_clock::now();

boost::chrono::high_resolution_clock::time_point t2
= boost::chrono::high_resolution_clock::now();

#endif // 0




using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

typedef websocketpp::client<websocketpp::config::asio_client> EchoClient;
typedef websocketpp::config::asio_client::message_type::ptr EchoMessage;

boost::mutex echoClientMutex;
EchoClient g_echoClient;
EchoClient::connection_ptr g_connecter;
boost::mutex *g_boxMutex;
DATA::StreamFrame *g_box;

void OnMessageHandler(EchoClient* client, websocketpp::connection_hdl hdl, EchoMessage msg) {
	echoClientMutex.lock();

	hdl.lock().get();

	std::string *copeMsg = new std::string(msg->get_payload());

	g_boxMutex->lock();
	g_box->PushBack(copeMsg);
	g_boxMutex->unlock();

	echoClientMutex.unlock();

	boost::chrono::high_resolution_clock::time_point t1 =
		boost::chrono::high_resolution_clock::now();

	boost::chrono::high_resolution_clock::time_point t2 =
		boost::chrono::high_resolution_clock::now();

#if 0
	t2 = t1;
	t1 = boost::chrono::high_resolution_clock::now();
		std::cout << boost::chrono::duration_cast<boost::chrono::nanoseconds>(t1 - t2) << "\n";
#endif // 0
}

void RunThread() {
	g_echoClient.connect(g_connecter);
	g_echoClient.run();
}

using namespace RECEIVER;

WS_ClientThread::WS_ClientThread()
	: _isInitialize(false), _thread(NULL) {
}

WS_ClientThread::WS_ClientThread(std::string url, DATA::StreamFrame *box, boost::mutex *boxMutex)
	: _url(url), _box(box), _boxMutex(boxMutex), _isInitialize(false), _thread(NULL) {
	SocketInit();
}

WS_ClientThread::~WS_ClientThread() {
	echoClientMutex.lock();

	if (_thread != NULL) {
		_thread->interrupt();
		delete _thread;
	}

	g_echoClient.stop();

	echoClientMutex.unlock();
}

void WS_ClientThread::SetUrl(std::string url) {
	_url = url;
}

void WS_ClientThread::SetBox(DATA::StreamFrame *box, boost::mutex *boxMutex) {
	_box = box;
	_boxMutex = boxMutex;
}

bool WS_ClientThread::SocketInit() {
	try {
		g_echoClient.set_access_channels(websocketpp::log::alevel::all);
		g_echoClient.clear_access_channels(websocketpp::log::alevel::frame_payload);
		g_echoClient.init_asio();
		g_echoClient.set_message_handler(bind(&OnMessageHandler, &g_echoClient, ::_1, ::_2));

		websocketpp::lib::error_code ec;
		g_connecter = g_echoClient.get_connection(_url, ec);
		if (!ec) {
			if (_boxMutex != NULL && _box != NULL) {
				g_box = _box;
				g_boxMutex = _boxMutex;
				_isInitialize = true;
			}
		}
		else {
			_isInitialize = false;
		}
	}
	catch (websocketpp::exception) {
		_isInitialize = false;
	}

	return _isInitialize;
}

bool WS_ClientThread::Run() {
	if (_isInitialize && _thread == NULL) {
		_thread = new boost::thread(&RunThread);
	}

	return _isInitialize;
}

void WS_ClientThread::Stop() {
	echoClientMutex.lock();

	_thread->interrupt();
	delete _thread;
	_thread = NULL;
	g_echoClient.stop();

	echoClientMutex.unlock();
}
