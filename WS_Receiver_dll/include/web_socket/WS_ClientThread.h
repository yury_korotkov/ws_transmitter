#pragma once

#include <receiver_dll.h>
#include <IncludeData.h>
#include <boost\thread.hpp>
#include <string>

namespace RECEIVER {

	class RECIVERDLL_API WS_ClientThread {
	public:
		WS_ClientThread();
		explicit WS_ClientThread(std::string url, DATA::StreamFrame *box, boost::mutex *boxMutex);
		virtual ~WS_ClientThread();

		void SetUrl(std::string url);
		void SetBox(DATA::StreamFrame *box, boost::mutex *boxMutex);
		bool SocketInit();

		bool Run();
		void Stop();

	private:
		

	private:
		std::string _url;
		DATA::StreamFrame *_box;
		boost::mutex *_boxMutex;
		bool _isInitialize;

		boost::thread *_thread;
	};

}

