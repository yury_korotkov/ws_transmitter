#include <IncludeData.h>
#include <IncludeReceiver.h>
#include <IncludeModel.h>

#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp> 

#include <iostream>
#include <string>

#pragma comment(lib, "WS_Receiver_dll.dll")

using namespace std;


int main(int argc, char* argv[]) {
	setlocale(LC_CTYPE, "rus");
	cout << fixed;
	cout.precision(6);

	DATA::StreamFrame *box = new DATA::StreamFrame;
	boost::mutex *boxMutex = new boost::mutex;
	DATA::MarketData *marketBox = new DATA::MarketData;
	boost::mutex *marketBoxMutex = new boost::mutex;


	RECEIVER::WS_ClientThread *client = new RECEIVER::WS_ClientThread("ws://api.hitbtc.com:80", box, boxMutex);
	client->Run();

	MODEL::Parser *parser = new MODEL::Parser(box, boxMutex, marketBox, marketBoxMutex);
	parser->Run();

	while (true) {
		boost::this_thread::sleep_for(boost::chrono::milliseconds(300));

		system("CLS");
		boxMutex->lock();
		cout << "�������: " << box->Size() << endl;
		boxMutex->unlock();

		marketBoxMutex->lock();
		for (DATA::TYPE::TradeSymbol index = DATA::TYPE::TradeSymbol(1); index < DATA::TYPE::TS_LAST; ++index) {
			DATA::TS_MarketData *thisData = marketBox->GetTradeSymbol(index);
			cout << setw(9) << thisData->GetTradeSymbol().GetNameStr() + string(":")
				<< " Bid: price: " << setw(13) << thisData->GetLessBid() /*<< " | volume: " << setw(13) << thisData->CountBid()*/
				<< " || Ask: price: " << setw(13) << thisData->GetMoreAsk() /*<< " | volume: " << setw(13) << thisData->CountAsk()*/ << endl;
				
		}
		marketBoxMutex->unlock();
	}

	client->Stop();
	parser->Stop();

	//boost::this_thread::sleep_for(boost::chrono::milliseconds(1000));

	delete client;
	delete parser;

	//boost::this_thread::sleep_for(boost::chrono::milliseconds(1000));

	return 0;

}